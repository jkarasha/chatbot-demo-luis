﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Bot.Builder.Azure;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

// For more information about this template visit http://aka.ms/azurebots-csharp-luis
[Serializable]
public class BasicLuisDialog : LuisDialog<object>
{
    public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(Utils.GetAppSetting("LuisAppId"), Utils.GetAppSetting("LuisAPIKey"))))
    {
    }

    [LuisIntent("None")]
    public async Task NoneIntent(IDialogContext context, LuisResult result)
    {
        await context.PostAsync($"You have reached the none intent. You said: {result.Query}"); //
        context.Wait(MessageReceived);
    }

    // Go to https://luis.ai and create a new intent, then train/publish your luis app.
    // Finally replace "MyIntent" with the name of your newly created intent in the following handler
    [LuisIntent("Greeting")]
    public async Task Greeting(IDialogContext context, LuisResult result)
    {
        await context.PostAsync($"Hi Joe, your last levels were high (150mg/dl). I can suggest meal plans for the day if you'd like."); //
        context.Wait(MessageReceived);
    }
    [LuisIntent("GetSnack")]
    public async Task GetSnack(IDialogContext context, LuisResult result)
    {
        var suggestion = GetSuggestion("snack");
        //var suggestion = "1 kiwi and 12 whole almonds";
        await context.PostAsync($"{suggestion}, might be a good healthy snack!"); //
        context.Wait(MessageReceived);
    }
    [LuisIntent("GetBreakfast")]
    public async Task GetBreakfast(IDialogContext context, LuisResult result)
    {
        var suggestion = GetSuggestion("breakfast");
        //var suggestion = "Crunchy yogurt";
        await context.PostAsync($"How about : {suggestion} to start the day?"); //
        context.Wait(MessageReceived);
    }
    [LuisIntent("GetLunch")]
    public async Task GetLunch(IDialogContext context, LuisResult result)
    {
        //var suggestion = GetSuggestion("lunch");
        var suggestion = "Bean tostada";
        await context.PostAsync($"{suggestion}, is a good hearty choice!"); //
        context.Wait(MessageReceived);
    }
    [LuisIntent("GetDinner")]
    public async Task GetDinner(IDialogContext context, LuisResult result)
    {
        //var suggestion = GetSuggestion("dinner");
        var suggestion = "Shrimp salad bowl";
        await context.PostAsync($"{suggestion}, is not a bad way to end the day!"); //
        context.Wait(MessageReceived);
    }

    public string GetSuggestion(string mealType)
    {
        string randomSuggestion;
        IList<Meal> snack = new List<Meal>();
        JArray items = GetMealData(mealType);
        //
        foreach (var item in items)
        {
            Meal meal = JsonConvert.DeserializeObject<Meal>(item.ToString());
            snack.Add(meal);
        }
        Random r = new Random();
        switch (mealType)
        {
            case "snack":
                randomSuggestion = snack[r.Next(snack.Count)].Description;
                break;
            default:
                randomSuggestion = snack[r.Next(snack.Count)].Title;
                break;
        }
        return randomSuggestion;
    }

    public static JArray GetMealData(string mealType)
    {

        StringBuilder mealData = new StringBuilder();
        mealData.Append("{");
        //breakfast
        mealData.Append("breakfast: [");
        mealData.Append("{'title': 'Crunchy yogurt', 'description': 'Combine 6 oz fat - free light yogurt, ¼ c granola cereal, 1 Tbsp ground flax seed, and 1 Tbsp chopped nuts.Add ground cinnamon and / or sugar substitute to taste.'},");
        mealData.Append("{'title': 'Eggs and English muffin','description': 'Scramble 1 egg in a pan coated with 1 tsp canola or olive oil; top with ¼ c chopped tomato, onion, and chile salsa.Serve with toasted 100 % whole grain English muffin, spread with 2 Tbsp low-fat(1 %) cottage cheese, and 1 c fat-free milk.'},");
        mealData.Append("{'title': 'Good Morning Blend','description': 'Stir together 6 ounces fat - free yogurt, 2 Tbsp dried mixed fruit, 2 Tbsp ground flax seed and 2 Tbsp chopped almonds, walnuts, or pecans.'},");
        mealData.Append("{'title': 'Nutty Oatmeal','description': 'Top ½ c cooked oatmeal with ¼ c walnuts or other nuts; add ground cinnamon and/ or sugar substitute to taste. Serve with 1 c fat-free milk or calcium - enriched soy or rice beverage.'}, ");
        mealData.Append("{'title': 'Bagel and cream cheese','description': 'Spread ½ 100 % whole grain bagel with 1 Tbsp low fat cream cheese.Serve with 1 c fat - free milk or calcium - enriched soy or rice beverage.'},");
        mealData.Append("{'title': 'Veggie omelet','description': 'Cook 1 egg white in a pan with 2 tsp canola, peanut or olive oil.Add ½ c spinach leaves, ½ c mushrooms, onions, garlic, and herbs as desired; and top with 2 Tbsp reduced fat cheese. Serve with 1 slice 100 % whole grain toast spread with 1 tsp canola-oil margarine and 1 c fat-free milk or calcium - enriched soy or rice beverage.'}");
        mealData.Append("],");
        //lunch
        mealData.Append("lunch: [");
        mealData.Append("{'title': 'Tuna melt','description': 'Top 1 toasted whole grain English muffin with ¼ c tuna mixed with 1 tsp mayonnaise (or 1 Tbsp light mayonnaise), 1 Tbsp minced dill pickle and/or chopped celery and 1 oz reduced-fat cheese. Place in pre-heated oven (450ºF) for 5 to 10 minutes (or microwave for 30 seconds until cheese melts).  Serve with 8 baby carrots with 2 Tbsp reduced fat ranch dressing, and 1 c fat-free milk or calcium-enriched soy beverage.'},");
        mealData.Append("{'title': 'Lean-body salad','description': 'Toss 2 c mixed dark greens, ½ c canned garbanzo beans (rinsed well), 1 oz reduced-fat Mozzarella shredded cheese and 2 Tbsp light Italian dressing. Serve with 1 fresh peach or ½ c canned peaches (in juice or water).'},");
        mealData.Append("{'title': 'Chicken salad','description': 'Combine 2 c mixed dark greens, 2 stalks chopped celery, and ¼ c sliced green or red grapes. Top with 2 oz cooked chicken breast, and drizzle with 2 Tbsp light honey mustard dressing (such as Newmans Own). Serve with 1 slice reduced-calorie 100% whole grain toast, spread with 1 tsp canola oil soft tub margarine.'},");
        mealData.Append("{'title': 'Roast-beef sandwich','description': 'Layer 2 oz lean roast beef, ½ c chopped romaine lettuce and ½ sliced tomato on 2 slices reduced calorie 100% whole grain bread, spread with 1 tsp mayonnaise and/or mustard.'},");
        mealData.Append("{'title': 'Pesto pizza','description': 'Split and toast a 100% whole grain English muffin. Top each half with 1 Tbsp pesto basil sauce, 1 slice tomato or ½ c canned tomatoes, and ½ slice  reduced-fat cheese. Broil or bake in oven until cheese melts.'},");
        mealData.Append("{'title': 'Bean tostada','description': 'Bake 1 corn tortilla in 400-degree oven until crisp. Spread with ½ c cooked or canned pinto beans (rinsed) and 2 Tbsp shredded reduced-fat Mexican blend cheese. Return to oven for 5 to 10 minutes until cheese melts. Top with ¼ c salsa. Serve with a cabbage salad (1 c shredded cabbage and 1 chopped tomato with 2 Tbsp reduced-fat dressing).'},");
        mealData.Append("{'title': 'Tuna salad','description': 'Mix 3 oz water-packed tuna with 2 stalks chopped celery, 4 chopped green olives, and 1 tsp regular (or1 T reduced-fat mayonnaise). Add 1 Tbsp seasoned rice vinegar, if desired. Scoop tuna onto 2 c mixed dark greens, and top with 1 Tbsp chopped almonds. Serve with 1 oz 100% whole grain crackers.'}");
        mealData.Append("],");
        //dinner
        mealData.Append("dinner: [");
        mealData.Append("{'title': 'Barbecue chicken','description': 'Grill or roast 3 oz chicken and top with 2 Tbsp barbecue sauce. Serve with 1 slice garlic sourdough toast, spread with1 tsp olive oil and garlic, and colorful coleslaw (mix 1 c shredded red and green cabbage and carrots with 1 Tbsp regular coleslaw dressing or 2 Tbsp reduced-fat dressing).'},");
        mealData.Append("{'title': 'Roast beef and rice','description': '3 oz lean roast beef, sliced, with ⅔ c cooked brown rice and 1 c cooked spinach, seasoned with 1 tsp olive oil and 1 tsp balsamic vinegar.'},");
        mealData.Append("{'title': 'Halibut and potatoes','description': '3 oz foil-baked halibut or other fish with 1 c green peppers and onions. Serve with ½ c red potatoes, roasted in 1 Tbsp olive oil and seasoned with herbs and spices.'},");
        mealData.Append("{'title': 'Pasta with meatballs','description': 'Toss 1 c cooked whole grain pasta in garlic and 1 Tbsp olive oil and garlic. Top with 3-oz lean meat balls (made with turkey, chicken or soy) and 1 tsp grated Parmesan cheese. Serve with cucumber salad (toss 1 c mixed greens, 1 c cucumber slices, 10 halved cherry tomatoes, ¼ c chopped red onions and 2 Tbsp reduced-fat Italian dressing).'},");
        mealData.Append("{'title': 'Shrimp salad bowl','description': 'Mix ⅓ c cooked brown rice and 2 Tbsp crumbled feta cheese. Scoop onto 2 c mixed greens, and top with 3 oz grilled or sautéed shrimp and 2 Tbsp reduced-fat dressing. Serve with 2 whole grain rye crispbread crackers, spread with 2 Tbsp low-fat ricotta or cottage cheese.'},");
        mealData.Append("{'title': 'Oven fried chicken','description': 'Toss 4 oz raw chicken breast in 1 Tbsp reduced-fat Italian dressing, coat with 2 Tbsp seasoned bread crumb and spray lightly with canola oil. Place on lightly oiled cookie sheet. Bake at 350ºF for 30 minutes or until browned and no longer pink inside. Serve with 3-bean salad (toss ½ c green beans, ¼  c garbanzo beans, ¼  c red beans, 2 Tbsp chopped onion and 2 Tbsp reduced-fat Italian dressing)'},");
        mealData.Append("{'title': 'Tofu stir fry','description': 'Stir-fry 3 oz tofu and 2 c mixed vegetables (broccoli, cauliflower, green beans, onions) in 2 Tbsp reduced sodium stir fry sauce and 1 Tbsp olive oil. Serve over ⅔ c cooked brown rice.'}");
        mealData.Append("],");
        //snack,
        mealData.Append("snack: [");
        mealData.Append("{'title': '','description': '1 medium orange or tangerine and 2 T dry roasted almonds (no added salt)'},");
        mealData.Append("{'title': '','description': '1 c fresh strawberries and ¼ c unsalted nuts'},");
        mealData.Append("{'title': '','description': '1 c seasonal melon and 6 oz fat-free light yogurt'},");
        mealData.Append("{'title': '','description': '4 dried apricot halves (or 3 dried plums) and 7 walnut halves'},");
        mealData.Append("{'title': '','description': '2 fresh or dried figs and ¼ c unsalted nuts'},");
        mealData.Append("{'title': '','description': '1 kiwi and 12 whole almonds'},");
        mealData.Append("{'title': '','description': '1 medium apple, sliced, with 2 Tbsp all-natural peanut butter'}");
        mealData.Append("]");
        //end root
        mealData.Append("}");
        
        JObject parsedData = JObject.Parse(mealData.ToString());
        return (JArray)parsedData[mealType];
    }
    public class Meal
    {
        public string Title { get; set; }
        public string Description { get; set; }

    }
}